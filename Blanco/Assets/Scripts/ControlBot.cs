using UnityEngine;
using UnityEngine.AI;
public class ControlBot : MonoBehaviour
{

    private GameObject jugador;
    public Transform Objetivo;
    public NavMeshAgent AI;
    public int rapidez;
    public UnityEngine.AI.NavMeshAgent Bot;
    void Start()
    {
        jugador = GameObject.Find("Jugador");


    }
    private void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        AI.SetDestination(Objetivo.position);
    }



}
